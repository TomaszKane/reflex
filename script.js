var result;
var targetSize = 100;

var stopAfter = 6;
var currentStep;
var punishment = 20;

var mainJq = $('#main');

var leftFrom = targetSize / 2;
var leftTo = mainJq.innerWidth() - (targetSize / 2);
var topFrom = targetSize / 2;
var topTo = mainJq.innerHeight() - (targetSize / 2);

var startButton = '<div class="start"><button>Start</button></div>';

console.log('left: ' + leftFrom + ':' + leftTo);
console.log('top: ' + topFrom + ':' + topTo);

$(function() {
    $(document).on('click', '.start button', function(event) {
        console.log('start');
        mainJq.empty();
        $('#step').text('Szczelaj w kółka!');
        event.stopPropagation();
        result = 0;
        currentStep = 1;
        draw(getLeft(), getTop());
    });
    $(document).on('click', '.target:not(.clicked)', function(event) {
        if ($(this).hasClass('clicked')) return;

        $(this).addClass('clicked');
        $(this).stop();
        event.stopPropagation();

        var points = targetSize - parseInt($(this).innerWidth());
        result += points;

        $('#step').text('Do końca: ' + (stopAfter - currentStep).toString());
        currentStep++;
        console.log('hit! ' + points);
        $(this).fadeOut('fast');
        if (currentStep <= stopAfter) {
            draw(getLeft(), getTop());
        } else {
            mainJq.empty();
            var resultDiv = '<div class="result">Twój wynik:<br><span>' + result + '</span></div>';
            mainJq.append(resultDiv);
            mainJq.append(startButton);
            mainJq.append('<p>Poszło ci: ' + getResultComment() + '</p>');
        }
    });
    $(document).on('click', '#main', function() {
        result -= punishment;
        console.log('punish! ' + punishment);
    });
});

function draw(left, top) {
    var w = 10;
    var h = 10;
    var target = $('<div class="target" style="width: ' + w + 'px; height: ' + h + 'px; top: ' + top + 'px; left: ' + left + 'px"></div>');

    target.appendTo(mainJq);

    target.animate({
        marginLeft: "-45",
        marginTop: "-45",
        height: targetSize,
        width: targetSize
    }, 3000, 'linear', function() {
        console.log('animation end!');
    });
}

function getLeft() {
    return [Math.random() * (leftTo - leftFrom) + leftFrom]
}
function getTop() {
    return [Math.random() * (topTo - topFrom) + topFrom]
}
function getResultComment() {
    var ratio = result / stopAfter;
    if (ratio > 84) return 'HaCer...';
    if (ratio > 78) return 'Nadczłowiek!';
    if (ratio > 70) return 'Zajebiście!';
    if (ratio > 62) return 'Dobrze';
    if (ratio > 54) return 'Nieźle';
    if (ratio > 46) return 'Przeciętnie';
    if (ratio > 36) return 'Słabo';
    if (ratio > 26) return 'Chujowo';
    if (ratio > 18) return 'No prawdziwy z ciebie zdolniacha';
    if (ratio > 8) return 'Pierwszy dzień przy komputerze?';
    if (ratio > 0) return 'Bliżej zera to się chyba nie dało...';
    if (ratio <= 0) return 'Nawet nie wiedziałem, że tak się da...';
}